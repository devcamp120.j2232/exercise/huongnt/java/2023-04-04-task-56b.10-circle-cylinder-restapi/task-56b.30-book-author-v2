package com.devcamp.bookauthorapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Author;
import com.devcamp.bookauthorapi.models.Book;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class BookApi {
    @GetMapping("/books")
    public ArrayList<Book> getBookList(){
        Author author1 = new Author("Margaret Mitchell", "Mitchell@gmail.com", 'f');
        Author author2 = new Author("Conan Doyle",'m');
        Author author3 = new Author("Agatha Christie",'f');
        Author author4 = new Author("William Shakespeare", "shakes@gmail.com", 'm');
        Author author5 = new Author("Nguyen Nhat Anh","anh@gmail.com",'m');
        Author author6 = new Author("Adrienne Kennedy",'f');
        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        System.out.println(author4);
        System.out.println(author5);
        System.out.println(author6);

        ArrayList<Author> AuthorList1 = new ArrayList<>();
        AuthorList1.add(author1);
        AuthorList1.add(author2);

        ArrayList<Author> AuthorList2 = new ArrayList<>();
        AuthorList2.add(author3);
        AuthorList2.add(author4);

        ArrayList<Author> AuthorList3 = new ArrayList<>();
        AuthorList3.add(author5);
        AuthorList3.add(author6);

        System.out.println(AuthorList1);
        System.out.println(AuthorList2);
        System.out.println(AuthorList3);


        Book book1 = new Book("Fiction Novel", AuthorList1, 200000.0, 1);
        Book book2 = new Book("Detective Novel",AuthorList2,300000.0, 2);
        Book book3 = new Book("Novel", AuthorList3, 280000.0, 3);

        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        //tạo danh sách ArrayList Book
        ArrayList<Book> BookList = new ArrayList<>();
        BookList.add(book1);
        BookList.add(book2);
        BookList.add(book3);
        System.out.println(BookList);
        return BookList;
     
        
    }
    
}
